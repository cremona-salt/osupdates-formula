# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import osupdates with context %}

osupdates-package-update-pkgs-uptodate:
  pkg.uptodate:
    - refresh: True
