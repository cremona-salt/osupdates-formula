.. _readme:

osupdates-formula
================

|img_travis| |img_sr|

.. |img_travis| image:: https://travis-ci.com/saltstack-formulas/osupdates-formula.svg?branch=master
   :alt: Travis CI Build Status
   :scale: 100%
   :target: https://travis-ci.com/saltstack-formulas/osupdates-formula
.. |img_sr| image:: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
   :alt: Semantic Release
   :scale: 100%
   :target: https://github.com/semantic-release/semantic-release

THIS REPOSITORY IS BEING RETIRED. SEE
`fishers-salt /
osupdates-formula <https://gitlab.com/fishers-salt/personal-formulas/osupdates-formula>_`
FOR THE REPLACEMENT PROJECT.

A SaltStack formula that is empty. It has dummy content to help with a quick
start on a new formula and it serves as a style guide.

.. contents:: **Table of Contents**
   :depth: 1

General notes
-------------

See the full `SaltStack Formulas installation and usage instructions
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

If you are interested in writing or contributing to formulas, please pay attention to the `Writing Formula Section
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#writing-formulas>`_.

If you want to use this formula, please pay attention to the ``FORMULA`` file and/or ``git tag``,
which contains the currently released version. This formula is versioned according to `Semantic Versioning <http://semver.org/>`_.

See `Formula Versioning Section <https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#versioning>`_ for more details.

If you need (non-default) configuration, please pay attention to the ``pillar.example`` file and/or `Special notes`_ section.

Contributing to this repo
-------------------------

**Commit message formatting is significant!!**

Please see `How to contribute <https://github.com/saltstack-formulas/.github/blob/master/CONTRIBUTING.rst>`_ for more details.

Special notes
-------------

None

Available states
----------------

.. contents::
   :local:

``osupdates``
^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This installs the osupdates package,
manages the osupdates configuration file and then
starts the associated osupdates service.

``osupdates.package``
^^^^^^^^^^^^^^^^^^^^

This state will install the osupdates package only.

``osupdates.config``
^^^^^^^^^^^^^^^^^^^

This state will configure the osupdates service and has a dependency on ``osupdates.install``
via include list.

``osupdates.service``
^^^^^^^^^^^^^^^^^^^^

This state will start the osupdates service and has a dependency on ``osupdates.config``
via include list.

``osupdates.clean``
^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

this state will undo everything performed in the ``osupdates`` meta-state in reverse order, i.e.
stops the service,
removes the configuration file and
then uninstalls the package.

``osupdates.service.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will stop the osupdates service and disable it at boot time.

``osupdates.config.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^

This state will remove the configuration of the osupdates service and has a
dependency on ``osupdates.service.clean`` via include list.

``osupdates.package.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will remove the osupdates package and has a depency on
``osupdates.config.clean`` via include list.

``osupdates.subcomponent``
^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state installs a subcomponent configuration file before
configuring and starting the osupdates service.

``osupdates.subcomponent.config``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will configure the osupdates subcomponent and has a
dependency on ``osupdates.config`` via include list.

``osupdates.subcomponent.config.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will remove the configuration of the osupdates subcomponent
and reload the osupdates service by a dependency on
``osupdates.service.running`` via include list and ``watch_in``
requisite.

Testing
-------

Linux testing is done with ``kitchen-salt``.

Requirements
^^^^^^^^^^^^

* Ruby
* Docker

.. code-block:: bash

   $ gem install bundler
   $ bundle install
   $ bin/kitchen test [platform]

Where ``[platform]`` is the platform name defined in ``kitchen.yml``,
e.g. ``debian-9-2019-2-py3``.

``bin/kitchen converge``
^^^^^^^^^^^^^^^^^^^^^^^^

Creates the docker instance and runs the ``osupdates`` main state, ready for testing.

``bin/kitchen verify``
^^^^^^^^^^^^^^^^^^^^^^

Runs the ``inspec`` tests on the actual instance.

``bin/kitchen destroy``
^^^^^^^^^^^^^^^^^^^^^^^

Removes the docker instance.

``bin/kitchen test``
^^^^^^^^^^^^^^^^^^^^

Runs all of the stages above in one go: i.e. ``destroy`` + ``converge`` + ``verify`` + ``destroy``.

``bin/kitchen login``
^^^^^^^^^^^^^^^^^^^^^

Gives you SSH access to the instance for manual testing.

